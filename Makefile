###############################################################################
### 		  University of Hawaii, College of Engineering
### @brief Lab 07d - animalFarm1 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Xiaokang Chen <xiaokang@hawaii.edu>
### @date 24 Feb 2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################


CC = g++
CFLAGS = -g -Wall -Wextra

TARGET = animalFarm1

all: $(TARGET)

config.o: config.c config.h
	$(CC) $(CFLAGS) -c config.c

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

addCats.o: addCats.c addCats.h
	$(CC) $(CFLAGS) -c addCats.c

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c

main.o: main.c config.h catDatabase.h addCats.h reportCats.h updateCats.h deleteCats.h
	$(CC) $(CFLAGS) -c main.c

animalFarm1: main.o config.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o config.o catDatabase.o addCats.o reportCats.o updateCats.o deleteCats.o

test: animalFarm1
	./animalFarm1

clean:
	rm -f $(TARGET) *.o
