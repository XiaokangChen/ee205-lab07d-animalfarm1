///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 07d - animalFarm1 - EE 205 - Spr 2022
///////
/////// @file addCats.h
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 24 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#pragma once


#include "catDatabase.h"


extern int addCat( const char name[], const int gender, const int breed, const bool isFixed, const float weight, const int collar1, const int collar2, const unsigned long long license );
