///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 07d - animalFarm1 - EE 205 - Spr 2022
///////
/////// @file catDatabase.c
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 24 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#include "catDatabase.h"

 
int currentCatListSize;
struct catMetaData cat[MAX_CAT - 1];
