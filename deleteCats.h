///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 07d - animalFarm1 - EE 205 - Spr 2022
///////
/////// @file deleteCats.h
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 24 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#pragma once


#include "catDatabase.h"


extern void deleteAllCats();
extern int  deleteCat( const int index );
