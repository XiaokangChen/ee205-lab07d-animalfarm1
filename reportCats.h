///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 07d - animalFarm1 - EE 205 - Spr 2022
///////
/////// @file reportCats.h
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 24 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#pragma once


#include "catDatabase.h"


extern int  printCat( const int index );
extern void printAllCats();
extern int  findCat( const char name[] );
